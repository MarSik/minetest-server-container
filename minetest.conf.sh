cat << EOF
ipv6_server = true
motd = ${MOTD}

# Admin user
name = ${ADMIN}

# No empty passwords
disallow_empty_password = true

enable_damage = ${DAMAGE}
creative_mode = ${CREATIVE}
enable_stamina = ${STAMINA}

enable_rollback_recording = true

whitelist.enable = ${WHITELIST_ENABLE}
whitelist.message = ${WHITELIST_MESSAGE}
EOF
